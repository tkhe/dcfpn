import torch.nn as nn
import torch.nn.functional as F

from detectron2.layers import Conv2d, ConvTranspose2d, ShapeSpec, get_norm
from detectron2.modeling.backbone import BACKBONE_REGISTRY
from detectron2.modeling.backbone import Backbone
from detectron2.modeling.backbone import build_resnet_backbone


class DCFPN(Backbone):
    def __init__(self, bottom_up, in_features, out_channels, norm=""):
        super().__init__()

        assert isinstance(bottom_up, Backbone)

        input_shapes = bottom_up.output_shape()
        in_channels = [input_shapes[f].channels for f in input_shapes]
        use_bias = norm == ""

        self.p6 = Conv2d(in_channels[-1], out_channels, 3, 2, 1)
        self.p7 = Conv2d(out_channels, out_channels, 3, 2, 1)

        self.i3 = Conv2d(in_channels[0], out_channels, 1, norm=get_norm(norm, out_channels), bias=use_bias)
        self.i4 = Conv2d(in_channels[1], out_channels, 1, norm=get_norm(norm, out_channels), bias=use_bias)
        self.i5 = Conv2d(in_channels[2], out_channels, 1, norm=get_norm(norm, out_channels), bias=use_bias)
        self.m3_4 = Conv2d(out_channels, out_channels, 3, 2, 1, norm=get_norm(norm, out_channels), bias=use_bias)
        self.m4_5 = Conv2d(out_channels, out_channels, 3, 2, 1, norm=get_norm(norm, out_channels), bias=use_bias)
        self.p5 = Conv2d(out_channels, out_channels, 3, 1, 1, norm=get_norm(norm, out_channels), bias=use_bias)

        self.bottom_up = bottom_up
        self.in_features = in_features
        self._out_channels = out_channels

    def forward(self, x):
        bottom_up_features = self.bottom_up(x)
        c3, c4, c5 = [bottom_up_features[f] for f in self.in_features]

        p6 = self.p6(c5)
        p7 = self.p7(p6)

        m3 = self.i3(c3)
        m4 = self.i4(c4) + self.m3_4(m3)
        m5 = self.i5(c5) + self.m4_5(m4)

        p5 = self.p5(m5) + m5
        p4 = F.interpolate(p5, scale_factor=2, mode="nearest") + m4
        p3 = F.interpolate(p4, scale_factor=2, mode="nearest") + m3

        return {
            "p3": p3, "p4": p4, "p5": p5, "p6": p6, "p7": p7
        }

    @property
    def size_divisibility(self):
        return 32

    def output_shape(self):
        return {
            f"p{i}": ShapeSpec(
                channels=self._out_channels,
                stride=2 ** i
            )
            for i in range(3, 8)
        }


class DCFPNP2(Backbone):
    def __init__(self, bottom_up, in_features, out_channels, norm=""):
        super().__init__()

        assert isinstance(bottom_up, Backbone)

        input_shapes = bottom_up.output_shape()
        in_channels = [input_shapes[f].channels for f in input_shapes]
        use_bias = norm == ""

        self.p6 = Conv2d(in_channels[-1], out_channels, 3, 2, 1)
        self.p7 = Conv2d(out_channels, out_channels, 3, 2, 1)

        self.i2 = Conv2d(in_channels[0], out_channels, 1, norm=get_norm(norm, out_channels), bias=use_bias)
        self.i3 = Conv2d(in_channels[1], out_channels, 1, norm=get_norm(norm, out_channels), bias=use_bias)
        self.i4 = Conv2d(in_channels[2], out_channels, 1, norm=get_norm(norm, out_channels), bias=use_bias)
        self.i5 = Conv2d(in_channels[3], out_channels, 1, norm=get_norm(norm, out_channels), bias=use_bias)
        self.m2_3 = Conv2d(out_channels, out_channels, 3, 2, 1, norm=get_norm(norm, out_channels), bias=use_bias)
        self.m3_4 = Conv2d(out_channels, out_channels, 3, 2, 1, norm=get_norm(norm, out_channels), bias=use_bias)
        self.m4_5 = Conv2d(out_channels, out_channels, 3, 2, 1, norm=get_norm(norm, out_channels), bias=use_bias)
        self.p5 = Conv2d(out_channels, out_channels, 3, 1, 1, norm=get_norm(norm, out_channels), bias=use_bias)

        self.bottom_up = bottom_up
        self.in_features = in_features
        self._out_channels = out_channels

    def forward(self, x):
        bottom_up_features = self.bottom_up(x)
        c2, c3, c4, c5 = [bottom_up_features[f] for f in self.in_features]
        results = []

        p6 = self.p6(c5)
        p7 = self.p7(p6)

        m2 = self.i2(c2)
        m3 = self.i3(c3) + self.m2_3(m2)
        m4 = self.i4(c4) + self.m3_4(m3)
        m5 = self.i5(c5) + self.m4_5(m4)

        p5 = self.p5(m5) + m5
        p4 = F.interpolate(p5, scale_factor=2, mode="nearest") + m4
        p3 = F.interpolate(p4, scale_factor=2, mode="nearest") + m3
        p2 = F.interpolate(p3, scale_factor=2, mode="nearest") + m2

        return {
            "p2": p2, "p3": p3, "p4": p4, "p5": p5, "p6": p6, "p7": p7
        }

    @property
    def size_divisibility(self):
        return 32

    def output_shape(self):
        return {
            f"p{i}": ShapeSpec(
                channels=self._out_channels,
                stride=2 ** i
            )
            for i in range(2, 8)
        }


@BACKBONE_REGISTRY.register()
def build_resnet_dcfpn_backbone(cfg, input_shape: ShapeSpec):
    bottom_up = build_resnet_backbone(cfg, input_shape)
    in_features = cfg.MODEL.DCFPN.IN_FEATURES
    out_channels = cfg.MODEL.DCFPN.OUT_CHANNELS
    backbone = DCFPN(
        bottom_up,
        in_features,
        out_channels,
        norm=cfg.MODEL.DCFPN.NORM
    )
    return backbone


@BACKBONE_REGISTRY.register()
def build_resnet_dcfpn_p2_backbone(cfg, input_shape: ShapeSpec):
    bottom_up = build_resnet_backbone(cfg, input_shape)
    in_features = cfg.MODEL.DCFPN.IN_FEATURES
    out_channels = cfg.MODEL.DCFPN.OUT_CHANNELS
    backbone = DCFPNP2(
        bottom_up,
        in_features,
        out_channels,
        norm=cfg.MODEL.DCFPN.NORM
    )
    return backbone
