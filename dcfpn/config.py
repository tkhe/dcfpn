from detectron2.config import CfgNode as CN


def add_dcfpn_config(cfg):
    _C = cfg

    _C.MODEL.DCFPN = CN()

    _C.MODEL.DCFPN.IN_FEATURES = []

    _C.MODEL.DCFPN.OUT_CHANNELS = 256

    _C.MODEL.DCFPN.NORM = ""
